package com.wm.renderers;

/**
 * Created by ionutvlad on 11.09.2016.
 */

import com.wm.enums.OptionsList;
import com.wm.repos.ColorsRepo;
import com.wm.repos.FontsRepo;

import javax.swing.*;
import java.awt.*;

/**
 *
 */
public final class OptionsRenderer extends JLabel implements ListCellRenderer<OptionsList>
{
    public OptionsRenderer()
    {
        this.setHorizontalAlignment(SwingUtilities.CENTER);
        this.setFont(FontsRepo.VERDANA_MEDIUM);
        this.setBackground(ColorsRepo.BRIGHT_BLUE);
        this.setForeground(ColorsRepo.WHITE);
        this.setBorder(BorderFactory.createLineBorder(ColorsRepo.WHITE));
    }
    
    @Override
    public Component getListCellRendererComponent(JList<? extends OptionsList> list, OptionsList value, int index, boolean isSelected, boolean cellHasFocus)
    {
        this.setText(value.toString());
        return this;
    }
}

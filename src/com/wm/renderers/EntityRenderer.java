package com.wm.renderers;

import com.wm.models.*;
import com.wm.repos.ColorsRepo;
import com.wm.services.LogService;
import com.wm.types.MessageType;
import com.wm.types.UserType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ionutvlad on 21.08.2016.
 */
public final class EntityRenderer extends JLabel implements ListCellRenderer<EntityModel>
{
    public EntityRenderer()
    {
        this.setOpaque(true);
        this.setBorder(BorderFactory.createLineBorder(Color.decode("#CFCFCF")));
    }
    
    private void actWhenActivity(final Activity activity)
    {
        setText(activity.getDescription());
    }
    
    private void actWhenProject(final Project project)
    {
        setText(project.getName());
    }
    
    private void actWhenTask(final Task task)
    {
        setText(task.getName());
    }
    
    private void actWhenTeam(final Team team)
    {
        setText(team.getName());
    }
    
    private void actWhenUser(final User user)
    {
        UserType type = UserType.values()[user.getType()];
        setIcon(new ImageIcon(type.toString()));
        setText(user.getName());
    }
    
    @Override
    public Component getListCellRendererComponent(JList<? extends EntityModel> list, EntityModel value, int index, boolean isSelected, boolean cellHasFocus)
    {
        if(isSelected)
        {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        }
        else
        {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        
        if(value instanceof Activity)
        {
            actWhenActivity((Activity) value);
            return this;
        }
        if(value instanceof Project)
        {
            actWhenProject((Project) value);
            return this;
        }
        if(value instanceof Task)
        {
            actWhenTask((Task) value);
            return this;
        }
        if(value instanceof Team)
        {
            actWhenTeam((Team) value);
            return this;
        }
        if(value instanceof User)
        {
            actWhenUser((User) value);
            return this;
        }
        return this;
    }
}

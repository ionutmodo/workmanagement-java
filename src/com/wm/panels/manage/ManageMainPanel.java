package com.wm.panels.manage;

import com.wm.repos.*;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ionutvlad on 18.08.2016.
 */
public abstract class ManageMainPanel extends JPanel
{
    public ManageMainPanel()
    {
        super();
        setup();
        createControls();
        addControls();
    }
    
    private void setup()
    {
        this.setBorder(BorderFactory.createLineBorder(ColorsRepo.BRIGHT_BLUE, 3));
        this.setLayout(null);
        this.setLocation(PointsRepo.MANAGE_PANEL);
        this.setSize(SizesRepo.MANAGE_PANEL);
        this.setVisible(false);
        this.setFont(FontsRepo.VERDANA_MEDIUM);
        this.setForeground(ColorsRepo.BLACK);
    }
    
    protected abstract void createControls();
    
    protected abstract void addControls();
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(ColorsRepo.DARK_BLUE);
        g.fillRect(0, 0, SizesRepo.MANAGE_RECTANGLE_W, SizesRepo.MANAGE_RECTANGLE_H);
        g.setFont(FontsRepo.VERDANA_SMALL);
        g.drawString(StringsRepo.COPYRIGHT, PointsRepo.MANAGE_COPYRIGHT_X, PointsRepo.MANAGE_COPYRIGHT_Y);
    }
}

package com.wm.panels.manage;

import com.wm.database.Database;
import com.wm.models.EntityModel;
import com.wm.models.Team;
import com.wm.models.User;
import com.wm.renderers.EntityRenderer;
import com.wm.repos.*;
import com.wm.services.CompService;
import com.wm.services.ValidateService;
import com.wm.types.QueryType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by ionutvlad on 17.08.2016.
 */
public final class ManageNewUserPanel extends ManageMainPanel
{
    private JTextField nameTextField, emailTextField, passwordTextField;
    private JRadioButton adminRadioButton, teamLeaderRadioButton, developerRadioButton;
    private JComboBox<Team> teamComboBox;
    private JButton createButton;
    
    public ManageNewUserPanel()
    {
        super();
    }
    
    private void addUser()
    {
        final String NAME_ERROR = "Name must be at least 6 characters long";
        final String EMAIL_ERROR = "Email must be at least 6 characters long";
        final String PASSWORD_ERROR = "Password must be at least 6 characters long";
        final String name = nameTextField.getText();
        final String email = emailTextField.getText();
        final String password = passwordTextField.getText();
        
        if(!ValidateService.minLength(name, 6, NAME_ERROR))
        {
            JOptionPane.showConfirmDialog(this, NAME_ERROR, "Info", JOptionPane.DEFAULT_OPTION);
            nameTextField.grabFocus();
            return;
        }
        if(!ValidateService.minLength(email, 6, EMAIL_ERROR))
        {
            JOptionPane.showConfirmDialog(this, EMAIL_ERROR, "Info", JOptionPane.DEFAULT_OPTION);
            emailTextField.grabFocus();
            return;
        }
        if(!ValidateService.minLength(password, 6, PASSWORD_ERROR))
        {
            JOptionPane.showConfirmDialog(this, PASSWORD_ERROR, "Info", JOptionPane.DEFAULT_OPTION);
            passwordTextField.grabFocus();
            return;
        }
        final byte type = getUserType();
        final int teamId = ((Team) teamComboBox.getSelectedItem()).getId();
        final User user = new User(0, name, email, password, null, type, teamId, 0, 0, false, null, null);
        Database.Insert.entityModel(user);
        clearGUI();
    }
    
    private void clearGUI()
    {
        nameTextField.setText(StringsRepo.EMPTY);
        emailTextField.setText(StringsRepo.EMPTY);
        passwordTextField.setText(StringsRepo.EMPTY);
        uncheckRadioButtons();
    }
    
    public void populateTeamComboBox()
    {
        EntityModel[] teams = Database.Select.getEntities(QueryType.TeamASC);
        for(EntityModel team : teams)
            teamComboBox.addItem((Team) team);
    }
    
    private ActionListener getActionListener()
    {
        return e ->
        {
            uncheckRadioButtons();
            JRadioButton radioButton = (JRadioButton) e.getSource();
            radioButton.setSelected(true);
        };
    }
    
    private byte getUserType()
    {
        if(adminRadioButton.isSelected())
            return 0;
        if(teamLeaderRadioButton.isSelected())
            return 1;
        if(developerRadioButton.isSelected())
            return 2;
        return -1;
    }
    
    private void uncheckRadioButtons()
    {
        adminRadioButton.setSelected(false);
        teamLeaderRadioButton.setSelected(false);
        developerRadioButton.setSelected(false);
    }
    
    @Override
    protected void createControls()
    {
        nameTextField = CompService.getTextField(PointsRepo.MANAGE_NEW_USER_NAME_TEXT, SizesRepo.MANAGE_NEW_USER_TEXT, false);
        emailTextField = CompService.getTextField(PointsRepo.MANAGE_NEW_USER_EMAIL_TEXT, SizesRepo.MANAGE_NEW_USER_TEXT, false);
        passwordTextField = CompService.getTextField(PointsRepo.MANAGE_NEW_USER_PASSWORD_TEXT, SizesRepo.MANAGE_NEW_USER_TEXT, true);
        
        ActionListener listener = getActionListener();
        adminRadioButton = CompService.getRadioButton("Admin", PointsRepo.MANAGE_NEW_USER_ADMIN_RADIO, SizesRepo.MANAGE_NEW_USER_RADIO);
        adminRadioButton.addActionListener(listener);
        adminRadioButton.setSelected(true);
        
        teamLeaderRadioButton = CompService.getRadioButton("Team Leader", PointsRepo.MANAGE_NEW_USER_LEADER_RADIO, SizesRepo.MANAGE_NEW_USER_RADIO);
        teamLeaderRadioButton.addActionListener(listener);
        
        developerRadioButton = CompService.getRadioButton("Developer", PointsRepo.MANAGE_NEW_USER_DEV_RADIO, SizesRepo.MANAGE_NEW_USER_RADIO);
        developerRadioButton.addActionListener(listener);
        
        teamComboBox = CompService.getComboBox(PointsRepo.MANAGE_NEW_USER_TEAM_COMBO, SizesRepo.MANAGE_NEW_USER_COMBO);
        teamComboBox.setRenderer(new EntityRenderer());
        
        createButton = CompService.getButton("Create user", PointsRepo.MANAGE_NEW_USER_BUTTON, SizesRepo.MANAGE_NEW_USER_CREATE);
        createButton.addActionListener(e -> addUser());
    }
    
    @Override
    protected void addControls()
    {
        this.add(CompService.getLabel("Name", PointsRepo.MANAGE_NEW_USER_NAME_LABEL, SizesRepo.MANAGE_NEW_USER_LABEL));
        this.add(CompService.getLabel("Email", PointsRepo.MANAGE_NEW_USER_EMAIL_LABEL, SizesRepo.MANAGE_NEW_USER_LABEL));
        this.add(CompService.getLabel("Password", PointsRepo.MANAGE_NEW_USER_PASSWORD_LABEL, SizesRepo.MANAGE_NEW_USER_LABEL));
        this.add(CompService.getLabel("Type", PointsRepo.MANAGE_NEW_USER_TYPE_LABEL, SizesRepo.MANAGE_NEW_USER_LABEL));
        this.add(CompService.getLabel("Team", PointsRepo.MANAGE_NEW_USER_TEAM_LABEL, SizesRepo.MANAGE_NEW_USER_LABEL));
        this.add(nameTextField);
        this.add(emailTextField);
        this.add(passwordTextField);
        this.add(adminRadioButton);
        this.add(teamLeaderRadioButton);
        this.add(developerRadioButton);
        this.add(teamComboBox);
        this.add(createButton);
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(ColorsRepo.WHITE);
        g.setFont(FontsRepo.VERDANA_BIG);
        g.drawString(StringsRepo.MANAGE_NEW_USER_TITLE, PointsRepo.MANAGE_NEW_USER_TITLE_X, PointsRepo.MANAGE_NEW_USER_TITLE_Y);
    }
}

package com.wm.panels.manage;

import com.wm.database.Database;
import com.wm.models.EntityModel;
import com.wm.models.User;
import com.wm.repos.*;
import com.wm.services.CompService;
import com.wm.types.QueryType;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ionutvlad on 20.08.2016.
 */
public final class ManageDeleteUserPanel extends ManageMainPanel
{
    private JList<EntityModel> list;
    private JTextField emailTextField;
    private JButton deleteButton;
    
    public ManageDeleteUserPanel()
    {
        super();
        updateUsersList();
    }
    
    public void updateUsersList()
    {
        list.removeAll();
        EntityModel[] usersList = Database.Select.getEntities(QueryType.UserTypeASC);
        list.setListData(usersList);
        if(usersList.length > 0)
        {
            list.setSelectedIndex(0);
            emailTextField.setText(User.class.cast(usersList[0]).getEmail());
        }
    }
    
    private void deleteUser()
    {
        User user = User.class.cast(list.getSelectedValue());
        Database.Delete.deleteUser(user);
        updateUsersList();
    }
    
    @Override
    protected void createControls()
    {
        list = CompService.getJList(PointsRepo.MANAGE_DELETE_USER_LIST, SizesRepo.MANAGE_DELETE_USER_LIST);
        list.addListSelectionListener(e ->
        {
            User user = User.class.cast(list.getSelectedValue());
            if(user != null)
                emailTextField.setText(user.getEmail());
        });
        
        emailTextField = CompService.getTextField(PointsRepo.MANAGE_DELETE_USER_TEXT, SizesRepo.MANAGE_DELETE_USER_TEXT, false);
        emailTextField.setEditable(false);
        
        deleteButton = CompService.getButton("Delete user", PointsRepo.MANAGE_DELETE_USER_BUTTON, SizesRepo.MANAGE_DELETE_USER_BUTTON);
        deleteButton.addActionListener(e -> deleteUser());
    }
    
    @Override
    protected void addControls()
    {
        this.add(CompService.getLabel("Email", PointsRepo.MANAGE_DELETE_USER_EMAIL_LABEL, SizesRepo.MANAGE_DELETE_USER_LABEL));
        this.add(emailTextField);
        this.add(deleteButton);
        this.add(list);
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(ColorsRepo.WHITE);
        g.setFont(FontsRepo.VERDANA_BIG);
        g.drawString(StringsRepo.MANAGE_DELETE_USER_TITLE, PointsRepo.MANAGE_DELETE_USER_TITLE_X, PointsRepo.MANAGE_DELETE_USER_TITLE_Y);
    }
}

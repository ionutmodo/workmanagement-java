package com.wm.panels.manage;

import com.wm.database.Database;
import com.wm.models.Project;
import com.wm.repos.*;
import com.wm.services.CompService;
import com.wm.services.ValidateService;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ionutvlad on 20.08.2016.
 */
public class ManageNewProjectPanel extends ManageMainPanel
{
    private JTextField nameTextField;
    private JButton createButton;
    
    public ManageNewProjectPanel()
    {
        super();
    }
    
    private void addProject()
    {
        final String name = nameTextField.getText();
        if(!ValidateService.minLength(name, 6, "Name must be at least 6 characters long"))
        {
            JOptionPane.showConfirmDialog(this, "Name must be at least 6 characters long", "Info", JOptionPane.DEFAULT_OPTION);
            nameTextField.grabFocus();
            return;
        }
        Database.Insert.entityModel(new Project(0, name));
        nameTextField.setText(StringsRepo.EMPTY);
    }
    
    @Override
    protected void createControls()
    {
        nameTextField = CompService.getTextField(PointsRepo.MANAGE_NEW_PROJECT_NAME_TEXT, SizesRepo.MANAGE_NEW_PROJECT_TEXT, false);
        createButton = CompService.getButton("Create project", PointsRepo.MANAGE_NEW_PROJECT_BUTTON, SizesRepo.MANAGE_NEW_PROJECT_BUTTON);
        createButton.addActionListener(e -> addProject());
    }
    
    @Override
    protected void addControls()
    {
        this.add(CompService.getLabel("Name", PointsRepo.MANAGE_NEW_PROJECT_NAME_LABEL, SizesRepo.MANAGE_NEW_PROJECT_LABEL));
        this.add(nameTextField);
        this.add(createButton);
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(ColorsRepo.WHITE);
        g.setFont(FontsRepo.VERDANA_BIG);
        g.drawString(StringsRepo.MANAGE_NEW_PROJECT_TITLE, PointsRepo.MANAGE_NEW_PROJECT_TITLE_X, PointsRepo.MANAGE_NEW_PROJECT_TITLE_Y);
    }
}

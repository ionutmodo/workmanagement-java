package com.wm.types;

/**
 * Created by ionutvlad on 22.08.2016.
 */
public enum UserType
{
    ADMIN("resources/admin.png"),
    LEAD("resources/lead.png"),
    DEV("resources/dev.png");
    
    private String path;
    
    UserType(final String path)
    {
        this.path = path;
    }
    
    @Override
    public String toString()
    {
        return path;
    }
}

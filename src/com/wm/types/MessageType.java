package com.wm.types;

/**
 * Created by ionutvlad on 11.08.2016.
 */
public enum MessageType
{
    ERR ("[  KO  ]"),
    WARN("[ WARN ]"),
    INFO("[ INFO ]"),
    OK  ("[  OK  ]");
    
    private String name;
    
    MessageType(final String name)
    {
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return name;
    }
}

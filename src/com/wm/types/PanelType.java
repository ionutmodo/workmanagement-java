package com.wm.types;

/**
 * Created by ionutvlad on 16.08.2016.
 */
public enum PanelType
{
    ManageNewUser,
    ManageDeleteUser,
    ManageNewProject
}

package com.wm.types;

/**
 * Created by ionutvlad on 21.08.2016.
 */
public enum QueryType
{
    ActivityASC("Activity", "FROM Activity ORDER BY start_time ASC"),
    ActivityDESC("Activity", "FROM Activity ORDER BY start_time DESC"),
    ProjectASC("Project", "FROM Project ORDER BY name ASC"),
    TeamASC("Team", "FROM Team ORDER BY name ASC"),
    UserNameASC("User", "FROM User ORDER BY name ASC"),
    UserTypeASC("User", "FROM User ORDER BY type ASC"),
    UserEmailASC("User", "FROM Activity ORDER BY email ASC");
    
    private String table;
    private String query;
    
    QueryType(String table, String query)
    {
        this.table = table;
        this.query = query;
    }
    
    public String getTable()
    {
        return table;
    }
    
    public String getQuery()
    {
        return query;
    }
}

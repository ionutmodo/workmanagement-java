package com.wm.main;

import com.wm.database.Database;
import com.wm.frames.LoginFrame;
import com.wm.frames.ManageFrame;
import com.wm.frames.WorkFrame;
import com.wm.repos.StringsRepo;
import com.wm.services.*;
import com.wm.types.MessageType;
/**
 * Created by ionutvlad on 11.08.2016.
 */
public class Main
{
    public static void main(final String[] args) throws Exception
    {
        Database.setup();
//        UtilService.removeBarButtons(LoginFrame.get());
//        LoginFrame.get().setVisible(true);
        
//        UtilService.removeBarButtons(ManageFrame.get());
//        ManageFrame.get().setVisible(true);
        
//        UtilService.removeBarButtons(WorkFrame.get());
        WorkFrame.get().setVisible(true);
    }
}

package com.wm.enums;

/**
 * Created by ionutvlad on 11.09.2016.
 */
public enum OptionsList
{
    Profile,
    Info,
    Logout,
    Exit;
    
    @Override
    public String toString()
    {
        return name();
    }
}

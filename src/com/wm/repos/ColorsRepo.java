package com.wm.repos;

import java.awt.*;

/**
 * Created by ionutvlad on 16.08.2016.
 */
public final class ColorsRepo
{
    public static final Color RED = Color.RED;
    public static final Color WHITE = Color.WHITE;
    public static final Color BLACK = Color.BLACK;
    public static final Color DARK_BLUE = Color.decode("#0C6BBB");
    public static final Color BRIGHT_BLUE = Color.decode("#4297dd");
}

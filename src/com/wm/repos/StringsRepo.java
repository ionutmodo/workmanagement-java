package com.wm.repos;

/**
 * Created by ionutvlad on 16.08.2016.
 */
public final class StringsRepo
{
    public static final String EMPTY = "";
    public static final String APP_NAME = "WorkManagement";
    public static final String APP_PATH = System.getProperty("deleteUser.dir");
    public static final String APP_RESOURCES = APP_PATH + "\\resources\\";
    public static final String COPYRIGHT = "Created by Ionut-Vlad Modoranu";
    
    public static final String MANAGE_NEW_USER_TITLE = "Create new user";
    public static final String MANAGE_DELETE_USER_TITLE = "Delete user by email";
    public static final String MANAGE_NEW_PROJECT_TITLE = "Create new project";
    public static final String WORK_TITLE = "WorkManagement - Work";
}

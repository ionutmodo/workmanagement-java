package com.wm.repos;

import java.awt.*;

/**
 * Created by ionutvlad on 16.08.2016.
 */
public final class FontsRepo
{
    public static final Font VERDANA_SMALL = new Font("Verdana", Font.BOLD, 9);
    public static final Font VERDANA_MEDIUM = new Font("Verdana", Font.BOLD, 13);
    public static final Font VERDANA_BIG = new Font("Verdana", Font.BOLD, 24);
}

package com.wm.repos;

import java.awt.*;

/**
 * Created by ionutvlad on 16.08.2016.
 */
public final class SizesRepo
{
    //region General constants
    public static final int TEXT_FIELD_MIN_SIZE = 6;
    public static final int TEXT_FIELD_MAX_SIZE = 32;
    //endregion
    
    //region LoginFrame dimensions
    public static final Dimension LOGIN_FRAME = new Dimension(400, 240);
    public static final Dimension LOGIN_EMAIL_TEXT = new Dimension(295, 25);
    public static final Dimension LOGIN_PASSWORD_TEXT = new Dimension(295, 25);
    public static final Dimension LOGIN_SHOW_CHECK = new Dimension(100, 20);
    public static final Dimension LOGIN_LOGIN_BUTTON = new Dimension(100, 30);
    public static final Dimension LOGIN_EXIT_BUTTON = new Dimension(100, 30);
    
    public static final Dimension LOGIN_EMAIL_LABEL = new Dimension(80, 20);
    public static final Dimension LOGIN_PASSWORD_LABEL = new Dimension(80, 20);
    
    public static final int LOGIN_RECTANGLE_W = 400;
    public static final int LOGIN_RECTANGLE_H = 70;
    //endregion
    
    //region Manage dimensions
    public static final Dimension MANAGE_FRAME = new Dimension(550, 420);
    public static final Dimension MANAGE_PANEL = new Dimension(544, 368);
    public static final Dimension MANAGE_NEW_USER_LABEL = new Dimension(80, 20);
    public static final Dimension MANAGE_NEW_USER_RADIO = new Dimension(130, 25);
    public static final Dimension MANAGE_NEW_USER_TEXT = new Dimension(300, 25);
    public static final Dimension MANAGE_NEW_USER_COMBO = new Dimension(200, 25);
    public static final Dimension MANAGE_NEW_USER_CREATE = new Dimension(150, 30);
    
    public static final int MANAGE_RECTANGLE_W = 550;
    public static final int MANAGE_RECTANGLE_H = 40;
    
    public static final Dimension MANAGE_DELETE_USER_LIST = new Dimension(455, 240);
    public static final Dimension MANAGE_DELETE_USER_LABEL = new Dimension(80, 20);
    public static final Dimension MANAGE_DELETE_USER_TEXT = new Dimension(300, 25);
    
    public static final Dimension MANAGE_DELETE_USER_BUTTON = new Dimension(150, 30);
    public static final Dimension MANAGE_NEW_PROJECT_LABEL = new Dimension(80,20);
    public static final Dimension MANAGE_NEW_PROJECT_TEXT = new Dimension(300, 25);
    public static final Dimension MANAGE_NEW_PROJECT_BUTTON = new Dimension(150, 30);
    //endregion
    
    //region WorkFrame dimensions
    public static final Dimension WORK_FRAME = new Dimension(800, 600);
    public static final Dimension WORK_LEFT_PANEL = new Dimension(610, 44);
    public static final Dimension WORK_RIGHT_PANEL = new Dimension(110, 44);
    public static final Dimension WORK_OPTIONS_LIST = new Dimension(110, 75);
    
    public static final int WORK_RECTANGLE_W = 800;
    public static final int WORK_RECTANGLE_H = 70;
    //endregion
}

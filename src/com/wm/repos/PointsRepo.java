package com.wm.repos;

import java.awt.*;

/**
 * Created by ionutvlad on 16.08.2016.
 */
public final class PointsRepo
{
    //public static final Point
    
    //region LoginFrame points
    public static final Point LOGIN_EMAIL_TEXT = new Point(90, 65);
    public static final Point LOGIN_PASSWORD_TEXT = new Point(90, 100);
    public static final Point LOGIN_SHOW_CHECK = new Point(270, 130);
    public static final Point LOGIN_LOGIN_BUTTON = new Point(90, 160);
    public static final Point LOGIN_EXIT_BUTTON = new Point(200, 160);
    
    public static final Point LOGIN_EMAIL_LABEL = new Point(10, 70);
    public static final Point LOGIN_PASSWORD_LABEL = new Point(10, 100);
    
    public static final int LOGIN_COPYRIGHT_X = 230;
    public static final int LOGIN_COPYRIGHT_Y = 230;
    
    public static final int LOGIN_TITLE_X = 90;
    public static final int LOGIN_TITLE_Y = 55;
    //endregion
    
    //region ManageFrame points
    public static final Point MANAGE_PANEL = new Point(0, 23);
    
    public static final Point MANAGE_NEW_USER_NAME_TEXT = new Point(160, 65);
    public static final Point MANAGE_NEW_USER_EMAIL_TEXT = new Point(160, 100);
    public static final Point MANAGE_NEW_USER_PASSWORD_TEXT = new Point(160, 135);
    
    public static final Point MANAGE_NEW_USER_ADMIN_RADIO = new Point(160, 170);
    public static final Point MANAGE_NEW_USER_LEADER_RADIO = new Point(160, 205);
    public static final Point MANAGE_NEW_USER_DEV_RADIO = new Point(160, 240);
    public static final Point MANAGE_NEW_USER_TEAM_COMBO = new Point(160, 275);
    
    public static final Point MANAGE_NEW_USER_NAME_LABEL = new Point(70, 65);
    public static final Point MANAGE_NEW_USER_EMAIL_LABEL = new Point(70, 100);
    public static final Point MANAGE_NEW_USER_PASSWORD_LABEL = new Point(70, 135);
    public static final Point MANAGE_NEW_USER_TYPE_LABEL = new Point(70, 205);
    public static final Point MANAGE_NEW_USER_TEAM_LABEL = new Point(70, 275);
    
    public static final Point MANAGE_NEW_USER_BUTTON = new Point(370, 310);
    
    public static final int MANAGE_NEW_USER_TITLE_X = 160;
    public static final int MANAGE_NEW_USER_TITLE_Y = 30;
    
    public static final int MANAGE_COPYRIGHT_X = 370;
    public static final int MANAGE_COPYRIGHT_Y = 360;
    
    // ManageDeleteUserPanel points
    public static final Point MANAGE_DELETE_USER_LIST = new Point(60, 60);
    public static final Point MANAGE_DELETE_USER_EMAIL_LABEL = new Point(10, 310);
    public static final Point MANAGE_DELETE_USER_TEXT = new Point(60, 310);
    public static final Point MANAGE_DELETE_USER_BUTTON = new Point(370, 310);
    
    public static final int MANAGE_DELETE_USER_TITLE_X = 135;
    public static final int MANAGE_DELETE_USER_TITLE_Y = 30;
    
    // ManageNewProjectPanel points
    public static final Point MANAGE_NEW_PROJECT_NAME_LABEL = new Point(70, 100);
    public static final Point MANAGE_NEW_PROJECT_NAME_TEXT = new Point(160, 100);
    public static final Point MANAGE_NEW_PROJECT_BUTTON = new Point(370, 310);
    
    public static final int MANAGE_NEW_PROJECT_TITLE_X = 150;
    public static final int MANAGE_NEW_PROJECT_TITLE_Y = 30;
    //endregion
    
    //region WorkFrame points
    public static final Point WORK_LEFT_PANEL = new Point(70, 0);
    public static final Point WORK_RIGHT_PANEL = new Point(680, 0);
    public static final Point WORK_OPTIONS_LIST = new Point(680, 44);
    
    public static final int WORK_TITLE_X = 15;
    public static final int WORK_TITLE_Y = 55;
    
    public static final int WORK_COPYRIGHT_X = 625;
    public static final int WORK_COPYRIGHT_Y = 590;
    //endregion
}

package com.wm.database;

import com.wm.database.interfaces.iUpdate;
import com.wm.models.User;
import com.wm.services.LogService;
import com.wm.services.UtilService;
import com.wm.types.MessageType;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ionutvlad on 27.08.2016.
 */
final class cUpdate implements iUpdate
{
    public User loginUser(final String email, final String password)
    {
        LogService.get().log(MessageType.INFO, "Logging in (" + email + ", " + password + ")");
        Session session = null;
        Transaction transaction = null;
        try
        {
            session = Database.FACTORY.openSession();
            transaction = session.beginTransaction();
            String SQL = "SELECT * FROM User WHERE email = :email AND password = :password";
            SQLQuery query = session.createSQLQuery(SQL);
            query.addEntity(User.class);
            query.setParameter("email", email);
            query.setParameter("password", password);
            List list = query.list();
            if(list.size() == 0)
            {
                LogService.get().log(MessageType.WARN, String.format("User '%s' doesn't exist", email));
                return null;
            }
            User user = (User) list.get(0);
            user.setOnline(true);
            user.setIpAddress(UtilService.getIP());
            user.setLastLogin(Timestamp.valueOf(LocalDateTime.now()));
            session.update(user);
            transaction.commit();
            LogService.get().log(MessageType.OK, String.format("User '%s' logged in", email));
            return user;
        }
        catch(HibernateException e)
        {
            if(transaction != null)
                transaction.rollback();
            LogService.get().log(MessageType.ERR, e);
            return null;
        }
        finally
        {
            if(session != null)
                session.close();
        }
    }
}

package com.wm.database;

import com.wm.database.interfaces.iDelete;
import com.wm.models.User;
import com.wm.services.LogService;
import com.wm.types.MessageType;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by ionutvlad on 27.08.2016.
 */
final class cDelete implements iDelete
{
    public boolean deleteUser(final User user)
    {
        LogService.get().log(MessageType.INFO, "Deleting deleteUser '" + user.getEmail() + "' from database");
        Session session = null;
        Transaction transaction = null;
        try
        {
            session = Database.FACTORY.openSession();
            transaction = session.beginTransaction();
            final String SQL = "DELETE FROM User WHERE id = :id";
            Query query = session.createQuery(SQL);
            query.setParameter("id", user.getId());
            LogService.get().log(MessageType.INFO, query.getQueryString());
            int affected = query.executeUpdate();
            transaction.commit();
            if(affected > 0)
            {
                LogService.get().log(MessageType.OK);
                return true;
            }
            LogService.get().log(MessageType.ERR, "User with email '" + user.getEmail() + "' doesn't exist");
            return false;
        }
        catch(HibernateException e)
        {
            LogService.get().log(MessageType.ERR, e);
            return false;
        }
        finally
        {
            if(session != null)
                session.close();
        }
    }
}

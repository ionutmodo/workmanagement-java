package com.wm.database;

import com.wm.database.interfaces.iDelete;
import com.wm.database.interfaces.iInsert;
import com.wm.database.interfaces.iSelect;
import com.wm.database.interfaces.iUpdate;
import com.wm.services.LogService;
import com.wm.types.MessageType;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Created by ionutvlad on 27.08.2016.
 */
public final class Database
{
    public static final iSelect Select = new cSelect();
    public static final iInsert Insert = new cInsert();
    public static final iUpdate Update = new cUpdate();
    public static final iDelete Delete = new cDelete();
    
    private static Database instance;
    
    public static Database get()
    {
        if(instance == null)
            instance = new Database();
        return instance;
    }
    
    protected static SessionFactory FACTORY;
    
    private Database() { }
    
    public static void setup()
    {
        try
        {
            LogService.get().log(MessageType.INFO, "Creating database connection objects");
        
            Configuration configuration = new Configuration();
            configuration.configure();
        
            ServiceRegistryBuilder registryBuilder = new ServiceRegistryBuilder();
            registryBuilder.applySettings(configuration.getProperties());
        
            ServiceRegistry serviceRegistry = registryBuilder.buildServiceRegistry();
        
            FACTORY = configuration.buildSessionFactory(serviceRegistry);
    
            LogService.get().log(MessageType.OK);
        }
        catch(HibernateException ex)
        {
            LogService.get().log(MessageType.ERR, ex);
        }
    }
}

package com.wm.database;

import com.wm.database.interfaces.iSelect;
import com.wm.models.EntityModel;
import com.wm.services.LogService;
import com.wm.types.MessageType;
import com.wm.types.QueryType;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ionutvlad on 27.08.2016.
 */
final class cSelect implements iSelect
{
    @SuppressWarnings("unchecked")
    public <T extends EntityModel> EntityModel[] getEntities(final QueryType queryType)
    {
        LogService.get().log(MessageType.INFO, "Querying all " + queryType.getTable() + " objects");
        Session session = null;
        EntityModel[] entities = null;
        try
        {
            session = Database.FACTORY.openSession();
            final Query query = session.createQuery(queryType.getQuery());
            List list = query.list();
            entities = new EntityModel[list.size()];
            for(int i = 0; i < list.size(); ++i)
                entities[i] = (T) list.get(i);
            LogService.get().log(MessageType.OK);
        }
        catch(HibernateException e)
        {
            LogService.get().log(MessageType.ERR, e);
        }
        finally
        {
            if(session != null)
                session.close();
        }
        return entities;
    }
}

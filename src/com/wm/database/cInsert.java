package com.wm.database;

import com.wm.database.interfaces.iInsert;
import com.wm.models.EntityModel;
import com.wm.services.LogService;
import com.wm.types.MessageType;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by ionutvlad on 27.08.2016.
 */
final class cInsert implements iInsert
{
    public Integer entityModel(final EntityModel entityModel)
    {
        LogService.get().log(MessageType.INFO, "Inserting " + entityModel.getTableName() + " object to database");
        Session session = null;
        Transaction transaction = null;
        Integer entityModelID = null;
        try
        {
            session = Database.FACTORY.openSession();
            transaction = session.beginTransaction();
            entityModelID = (Integer) session.save(entityModel);
            transaction.commit();
            session.flush();
            LogService.get().log(MessageType.OK);
        }
        catch(HibernateException e)
        {
            if(transaction != null)
                transaction.rollback();
            LogService.get().log(MessageType.ERR, e);
        }
        finally
        {
            if(session != null)
                session.close();
        }
        return entityModelID;
    }
}

package com.wm.database.interfaces;

import com.wm.models.User;

/**
 * Created by ionutvlad on 27.08.2016.
 */
public interface iDelete
{
    boolean deleteUser(final User user);
}

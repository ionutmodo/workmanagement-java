package com.wm.database.interfaces;

import com.wm.models.EntityModel;

/**
 * Created by ionutvlad on 27.08.2016.
 */
public interface iInsert
{
    Integer entityModel(final EntityModel entityModel);
}

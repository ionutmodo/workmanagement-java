package com.wm.database.interfaces;

import com.wm.models.EntityModel;
import com.wm.types.QueryType;

import java.util.List;

/**
 * Created by ionutvlad on 27.08.2016.
 */
public interface iSelect
{
    <T extends EntityModel> EntityModel[] getEntities(final QueryType queryType);
}

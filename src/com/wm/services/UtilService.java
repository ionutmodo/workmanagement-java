package com.wm.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;

/**
 * Created by ionutvlad on 17.08.2016.
 */
public final class UtilService
{
    public static boolean classContains(final Class c, final String fieldName)
    {
        Field[] fields = c.getDeclaredFields();
        for(Field f : fields)
            if(fieldName.equals(f.getName()))
                return true;
        return false;
    }
    
    public static String getIP()
    {
        try
        {
            URL link = new URL("http://checkip.amazonaws.com");
            BufferedReader reader = new BufferedReader(new InputStreamReader(link.openStream()));
            String IP = reader.readLine();
            reader.close();
            return IP;
        }
        catch(IOException e)
        {
            return null;
        }
    }
}

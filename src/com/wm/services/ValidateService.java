package com.wm.services;

import com.wm.types.MessageType;

/**
 * Created by ionutvlad on 18.08.2016.
 */
public final class ValidateService
{
    public static boolean notNull(Object o, String message)
    {
        if(o == null)
        {
            LogService.get().log(MessageType.ERR, message);
            return false;
        }
        return true;
    }
    
    public static boolean isTrue(boolean expression, String message)
    {
        if(!expression)
        {
            LogService.get().log(MessageType.ERR, message);
            return false;
        }
        return true;
    }
    
    public static boolean isFalse(boolean expression, String message)
    {
        return isTrue(!expression, message);
    }
    
    public static boolean minLength(String str, int minLength, String message)
    {
        if(str.length() < minLength)
        {
            LogService.get().log(MessageType.ERR, message);
            return false;
        }
        return true;
    }
    
    public static boolean maxLength(String str, int maxLength, String message)
    {
        if(str.length() > maxLength)
        {
            LogService.get().log(MessageType.ERR, message);
            return false;
        }
        return true;
    }
}

package com.wm.services;

import com.wm.repos.StringsRepo;
import com.wm.types.MessageType;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Created by ionutvlad on 13.08.2016.
 */
public final class TrayService
{
    private static TrayService instance;
    
    public static TrayService get()
    {
        if(instance == null)
            instance = new TrayService();
        return instance;
    }
    
    private TrayIcon trayIcon;
    private Popup popup;
    
    private TrayService()
    {
        if(!SystemTray.isSupported())
        {
            LogService.get().log(MessageType.INFO, "TrayIcon cannot be added to SystemTray");
        }
        else
        {
            Image icon = getIcon();
            if(icon == null)
                LogService.get().log(MessageType.ERR, "Image icon.gif was not found in resources folder");
            else
            {
                trayIcon = new TrayIcon(getIcon());
                try
                {
                    SystemTray.getSystemTray().add(trayIcon);
                }
                catch(AWTException e)
                {
                    LogService.get().log(MessageType.ERR, "Error while adding TrayIcon to SystemTray");
                    LogService.get().log(MessageType.ERR, e.toString());
                }
            }
        }
    }
    
    private Image getIcon()
    {
        final String path = StringsRepo.APP_RESOURCES + "bulb.gif";
        URL imageURL = TrayService.class.getResource(path);
        if(imageURL == null)
            return null;
        return new ImageIcon(imageURL, StringsRepo.APP_NAME).getImage();
    }
}

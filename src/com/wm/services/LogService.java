package com.wm.services;

import com.wm.types.MessageType;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ionutvlad on 09.08.2016.
 */
public final class LogService
{
    private static LogService instance;
    
    public static LogService get()
    {
        if(instance == null)
            instance = new LogService();
        return instance;
    }
    
    private BufferedWriter writer = null;
    
    private LogService()
    {
        final String FILENAME = "logs\\" + getDateTime(true) + ".log";
        try
        {
            writer = new BufferedWriter(new FileWriter(new File(FILENAME)));
            log(MessageType.INFO, "Created log file");
        }
        catch(IOException e)
        {
            logConsole(MessageType.ERR, "Couldn't create log file \"" + FILENAME + "\"");
        }
    }
    
    public void close()
    {
        try
        {
            writer.close();
        }
        catch(IOException e)
        {
            logConsole(MessageType.ERR, "Couldn't close logger!");
        }
    }
    
    public void log(final MessageType messageType, final String text)
    {
        logConsole(messageType, text);
        logFile(messageType, text);
    }
    
    public void log(final MessageType messageType)
    {
        if(messageType == MessageType.OK)
            log(messageType, "Success");
    }
    
    public void log(final MessageType messageType, Exception e)
    {
        log(messageType, e.toString());
        for(StackTraceElement ste : e.getStackTrace())
        {
            System.out.println("\t" + ste.toString());
        }
    }
    
    private void logConsole(final MessageType messageType, final String text)
    {
        System.out.println(getDateTime(false) + " " + messageType + " > " + text);
    }
    
    private void logFile(final MessageType messageType, final String text)
    {
        try
        {
            writer.write(getDateTime(false) + " " + messageType + " > " + text);
            writer.newLine();
        }
        catch(IOException e)
        {
            logConsole(MessageType.ERR, "Couldn't write to log file!");
        }
    }
    
    private String getDateTime(final boolean onlyDate)
    {
        final String pattern = onlyDate ? "yyyy-MM-dd" : "[yyyy-MM-dd@HH:mm:ss]";
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        return dateFormat.format(date);
    }
}

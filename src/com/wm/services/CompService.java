package com.wm.services;

import com.wm.models.EntityModel;
import com.wm.renderers.EntityRenderer;
import com.wm.repos.ColorsRepo;
import com.wm.repos.FontsRepo;
import com.wm.repos.SizesRepo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by ionutvlad on 11.08.2016.
 */
public final class CompService
{
    private CompService() { }
    
    public static JLabel getLabel(final String name, final Point location, final Dimension size)
    {
        JLabel label = new JLabel(name);
        if(size == null)
            label.setForeground(ColorsRepo.WHITE);
        else
        {
            label.setSize(size);
            label.setForeground(ColorsRepo.BLACK);
        }
        if(location != null)
            label.setLocation(location);
        label.setFont(FontsRepo.VERDANA_MEDIUM);
        return label;
    }
    
    public static JCheckBox getCheckBox(final String text, final Point location, final Dimension size)
    {
        JCheckBox checkBox = new JCheckBox(text);
        checkBox.setSize(size);
        checkBox.setLocation(location);
        checkBox.setFont(FontsRepo.VERDANA_MEDIUM);
        checkBox.setSelected(false);
        return checkBox;
    }
    
    public static <T extends EntityModel> JComboBox<T> getComboBox(final Point location, final Dimension size)
    {
        JComboBox<T> comboBox = new JComboBox<>();
        comboBox.setLocation(location);
        comboBox.setSize(size);
        comboBox.setFont(FontsRepo.VERDANA_MEDIUM);
        comboBox.setForeground(ColorsRepo.BLACK);
        comboBox.setRequestFocusEnabled(false);
        comboBox.setFocusable(false);
        return comboBox;
    }
    
    public static JRadioButton getRadioButton(final String text, final Point location, final Dimension size)
    {
        JRadioButton radioButton = new JRadioButton(text);
        radioButton.setSize(size);
        radioButton.setLocation(location);
        radioButton.setFont(FontsRepo.VERDANA_MEDIUM);
        radioButton.setForeground(ColorsRepo.BLACK);
        radioButton.setBorderPainted(false);
        radioButton.setFocusPainted(false);
        return radioButton;
    }
    
    public static JButton getButton(final String text, final Point location, final Dimension size)
    {
        JButton button = new JButton(text);
        button.setVisible(true);
        if(size != null)
            button.setSize(size);
        if(location != null)
            button.setLocation(location);
        button.setFont(FontsRepo.VERDANA_MEDIUM);
        button.setForeground(Color.white);
        button.setBackground(ColorsRepo.DARK_BLUE);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseEntered(MouseEvent e)
            {
                button.setBackground(ColorsRepo.BRIGHT_BLUE);
            }
            
            @Override
            public void mouseExited(MouseEvent e)
            {
                button.setBackground(ColorsRepo.DARK_BLUE);
            }
        });
        return button;
    }
    
    public static JTextField getTextField(final Point location, final Dimension size, final boolean isPassword)
    {
        JTextField textField = new JTextField();
        if(isPassword)
        {
            textField = new JPasswordField();
            ((JPasswordField) textField).setEchoChar('*');
        }
        textField.addKeyListener(getKeyAdapter(textField));
        textField.setSize(size);
        textField.setLocation(location);
        textField.setFont(FontsRepo.VERDANA_MEDIUM);
        textField.setForeground(ColorsRepo.RED);
        return textField;
    }
    
    public static <T extends EntityModel> JList<T> getJList(final Point location, final Dimension size)
    {
        JList<T> list = new JList<>();
        list.setLocation(location);
        list.setSize(size);
        list.setFont(FontsRepo.VERDANA_MEDIUM);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setCellRenderer(new EntityRenderer());
        list.setBorder(BorderFactory.createLineBorder(ColorsRepo.BLACK));
        list.setForeground(ColorsRepo.BLACK);
        list.setAutoscrolls(true);
        return list;
    }
    
    private static KeyAdapter getKeyAdapter(final JTextField textField)
    {
        return new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                final int length = textField.getText().length();
                if(length == SizesRepo.TEXT_FIELD_MAX_SIZE)
                    e.consume();
                
                final boolean underMinimum = (length < SizesRepo.TEXT_FIELD_MIN_SIZE);
                textField.setForeground(underMinimum ? ColorsRepo.RED : ColorsRepo.BLACK);
    
                final int code = e.getKeyCode();
                if((code == 8 || code == 127) && underMinimum)
                    textField.setForeground(ColorsRepo.RED);
            }
        };
    }
}

package com.wm.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by ionutvlad on 16.08.2016.
 */
@Entity
public class Activity extends EntityModel
{
    private int id;
    private int userId;
    private int projectId;
    private int taskId;
    private Timestamp startTime;
    private long duration;
    private String description;
    private boolean isActive;
    
    public Activity()
    {
        super("Activity");
    }
    
    public Activity(int id, int userId, int projectId, int taskId, Timestamp startTime, long duration, String description, boolean isActive)
    {
        super("Activity");
        this.id = id;
        this.userId = userId;
        this.projectId = projectId;
        this.taskId = taskId;
        this.startTime = startTime;
        this.duration = duration;
        this.description = description;
        this.isActive = isActive;
    }
    
    @Id
    @Column(name = "id", nullable = false)
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    @Basic
    @Column(name = "user_id", nullable = false)
    public int getUserId()
    {
        return userId;
    }
    
    public void setUserId(int userId)
    {
        this.userId = userId;
    }
    
    @Basic
    @Column(name = "project_id", nullable = false)
    public int getProjectId()
    {
        return projectId;
    }
    
    public void setProjectId(int projectId)
    {
        this.projectId = projectId;
    }
    
    @Basic
    @Column(name = "task_id", nullable = false)
    public int getTaskId()
    {
        return taskId;
    }
    
    public void setTaskId(int taskId)
    {
        this.taskId = taskId;
    }
    
    @Basic
    @Column(name = "start_time", nullable = false)
    public Timestamp getStartTime()
    {
        return new Timestamp(startTime.getTime());
    }
    
    public void setStartTime(Timestamp startTime)
    {
        this.startTime = startTime;
    }
    
    @Basic
    @Column(name = "duration", nullable = false)
    public long getDuration()
    {
        return duration;
    }
    
    public void setDuration(long duration)
    {
        this.duration = duration;
    }
    
    @Basic
    @Column(name = "description", nullable = false, length = -1)
    public String getDescription()
    {
        return new String(description);
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    @Basic
    @Column(name = "isActive", nullable = false)
    public boolean isActive()
    {
        return isActive;
    }
    
    public void setActive(boolean active)
    {
        isActive = active;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        
        Activity activity = (Activity) o;
        
        if(id != activity.id) return false;
        if(userId != activity.userId) return false;
        if(projectId != activity.projectId) return false;
        if(taskId != activity.taskId) return false;
        if(duration != activity.duration) return false;
        if(isActive != activity.isActive) return false;
        if(startTime != null ? !startTime.equals(activity.startTime) : activity.startTime != null) return false;
        if(description != null ? !description.equals(activity.description) : activity.description != null) return false;
        
        return true;
    }
    
    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + projectId;
        result = 31 * result + taskId;
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (int) (duration ^ (duration >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (isActive ? 1 : 0);
        return result;
    }
    
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(id).append(" | ");
        builder.append(userId).append(" | ");
        builder.append(projectId).append(" | ");
        builder.append(taskId).append(" | ");
        builder.append(startTime).append(" | ");
        builder.append(duration).append(" | ");
        builder.append(description).append(" | ");
        builder.append(isActive);
        return builder.toString();
    }
}

package com.wm.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by ionutvlad on 16.08.2016.
 */
@Entity
public class Task extends EntityModel
{
    private int id;
    private String name;
    private String description;
    private int projectId;
    private int creatorId;
    private int solverId;
    private int teamId;
    private boolean isActive;
    
    public Task()
    {
        super("Task");
    }
    
    public Task(int id, String name, String description, int projectId, int creatorId, int solverId, int teamId, boolean isActive)
    {
        super("Task");
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.creatorId = creatorId;
        this.solverId = solverId;
        this.teamId = teamId;
        this.isActive = isActive;
    }
    
    @Id
    @Column(name = "id", nullable = false)
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName()
    {
        return new String(name);
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    @Basic
    @Column(name = "description", nullable = false, length = -1)
    public String getDescription()
    {
        return new String(description);
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    @Basic
    @Column(name = "project_id", nullable = false)
    public int getProjectId()
    {
        return projectId;
    }
    
    public void setProjectId(int projectId)
    {
        this.projectId = projectId;
    }
    
    @Basic
    @Column(name = "creator_id", nullable = false)
    public int getCreatorId()
    {
        return creatorId;
    }
    
    public void setCreatorId(int creatorId)
    {
        this.creatorId = creatorId;
    }
    
    @Basic
    @Column(name = "solver_id", nullable = false)
    public int getSolverId()
    {
        return solverId;
    }
    
    public void setSolverId(int solverId)
    {
        this.solverId = solverId;
    }
    
    @Basic
    @Column(name = "team_id", nullable = false)
    public int getTeamId()
    {
        return teamId;
    }
    
    public void setTeamId(int teamId)
    {
        this.teamId = teamId;
    }
    
    @Basic
    @Column(name = "isActive", nullable = false)
    public boolean isActive()
    {
        return isActive;
    }
    
    public void setActive(boolean active)
    {
        isActive = active;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        
        Task task = (Task) o;
        
        if(id != task.id) return false;
        if(creatorId != task.creatorId) return false;
        if(solverId != task.solverId) return false;
        if(teamId != task.teamId) return false;
        if(isActive != task.isActive) return false;
        if(name != null ? !name.equals(task.name) : task.name != null) return false;
        if(description != null ? !description.equals(task.description) : task.description != null) return false;
        
        return true;
    }
    
    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + creatorId;
        result = 31 * result + solverId;
        result = 31 * result + teamId;
        result = 31 * result + (isActive ? 1 : 0);
        return result;
    }
    
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(id).append(" | ");
        builder.append(name).append(" | ");
        builder.append(description).append(" | ");
        builder.append(creatorId).append(" | ");
        builder.append(solverId).append(" | ");
        builder.append(teamId).append(" | ");
        builder.append(isActive);
        return builder.toString();
    }
}

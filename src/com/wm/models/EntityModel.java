package com.wm.models;

/**
 * Created by ionutvlad on 10.08.2016.
 */
public abstract class EntityModel
{
    protected final String tableName;
    
    public EntityModel(final String tableName)
    {
        this.tableName = tableName;
    }
    
    public String getTableName()
    {
        return tableName;
    }
}

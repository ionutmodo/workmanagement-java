package com.wm.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by ionutvlad on 16.08.2016.
 */
@Entity
public class User extends EntityModel
{
    private int id;
    private String name;
    private String email;
    private String password;
    private Timestamp lastLogin;
    private byte type;
    private int teamId;
    private int workTime;
    private long pauseTime;
    private boolean isOnline;
    private String imagePath;
    private String ipAddress;
    
    public User()
    {
        super("User");
    }
    
    public User(int id, String name, String email, String password, Timestamp lastLogin, byte type, int teamId, int workTime, long pauseTime, boolean isOnline, String imagePath, String ipAddress)
    {
        super("User");
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.lastLogin = lastLogin;
        this.type = type;
        this.teamId = teamId;
        this.workTime = workTime;
        this.pauseTime = pauseTime;
        this.isOnline = isOnline;
        this.imagePath = imagePath;
        this.ipAddress = ipAddress;
    }
    
    @Id
    @Column(name = "id", nullable = false)
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName()
    {
        if(name == null)
            return null;
        return new String(name);
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    @Basic
    @Column(name = "email", nullable = false, length = 32)
    public String getEmail()
    {
        if(email == null)
            return null;
        return new String(email);
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    @Basic
    @Column(name = "password", nullable = false, length = 32)
    public String getPassword()
    {
        if(password == null)
            return null;
        return new String(password);
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    @Basic
    @Column(name = "last_login", nullable = true)
    public Timestamp getLastLogin()
    {
        if(lastLogin == null)
            return null;
        return new Timestamp(lastLogin.getTime());
    }
    
    public void setLastLogin(Timestamp lastLogin)
    {
        this.lastLogin = lastLogin;
    }
    
    @Basic
    @Column(name = "type", nullable = false)
    public byte getType()
    {
        return type;
    }
    
    public void setType(byte type)
    {
        this.type = type;
    }
    
    @Basic
    @Column(name = "team_id", nullable = false)
    public int getTeamId()
    {
        return teamId;
    }
    
    public void setTeamId(int teamId)
    {
        this.teamId = teamId;
    }
    
    @Basic
    @Column(name = "work_time", nullable = false)
    public int getWorkTime()
    {
        return workTime;
    }
    
    public void setWorkTime(int workTime)
    {
        this.workTime = workTime;
    }
    
    @Basic
    @Column(name = "pause_time", nullable = false)
    public long getPauseTime()
    {
        return pauseTime;
    }
    
    public void setPauseTime(long pauseTime)
    {
        this.pauseTime = pauseTime;
    }
    
    @Basic
    @Column(name = "isOnline", nullable = false)
    public boolean isOnline()
    {
        return isOnline;
    }
    
    public void setOnline(boolean online)
    {
        isOnline = online;
    }
    
    @Basic
    @Column(name = "imagePath", nullable = true, length = 64)
    public String getImagePath()
    {
        if(imagePath == null)
            return null;
        return new String(imagePath);
    }
    
    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }
    
    @Basic
    @Column(name = "ipAddress", nullable = true, length = 16)
    public String getIpAddress()
    {
        if(ipAddress == null)
            return null;
        return new String(ipAddress);
    }
    
    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        
        User user = (User) o;
        
        if(id != user.id) return false;
        if(type != user.type) return false;
        if(teamId != user.teamId) return false;
        if(workTime != user.workTime) return false;
        if(pauseTime != user.pauseTime) return false;
        if(isOnline != user.isOnline) return false;
        if(name != null ? !name.equals(user.name) : user.name != null) return false;
        if(email != null ? !email.equals(user.email) : user.email != null) return false;
        if(password != null ? !password.equals(user.password) : user.password != null) return false;
        if(lastLogin != null ? !lastLogin.equals(user.lastLogin) : user.lastLogin != null) return false;
        if(imagePath != null ? !imagePath.equals(user.imagePath) : user.imagePath != null) return false;
        if(ipAddress != null ? !ipAddress.equals(user.ipAddress) : user.ipAddress != null) return false;
        
        return true;
    }
    
    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (lastLogin != null ? lastLogin.hashCode() : 0);
        result = 31 * result + (int) type;
        result = 31 * result + teamId;
        result = 31 * result + workTime;
        result = 31 * result + (int) (pauseTime ^ (pauseTime >>> 32));
        result = 31 * result + (isOnline ? 1 : 0);
        result = 31 * result + (imagePath != null ? imagePath.hashCode() : 0);
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(id).append(" | ");
        builder.append(name).append(" | ");
        builder.append(email).append(" | ");
        builder.append(password).append(" | ");
        builder.append(lastLogin).append(" | ");
        builder.append(type).append(" | ");
        builder.append(teamId).append(" | ");
        builder.append(workTime).append(" | ");
        builder.append(pauseTime).append(" | ");
        builder.append(isOnline).append(" | ");
        builder.append(imagePath).append(" | ");
        builder.append(ipAddress);
        return builder.toString();
    }
}

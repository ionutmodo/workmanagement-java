package com.wm.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by ionutvlad on 16.08.2016.
 */
@Entity
public class Team extends EntityModel
{
    private int id;
    private String name;
    
    public Team()
    {
        super("Team");
    }
    
    public Team(int id, String name)
    {
        super("Team");
        this.id = id;
        this.name = name;
    }
    
    @Id
    @Column(name = "id", nullable = false)
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName()
    {
        return new String(name);
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        
        Team team = (Team) o;
        
        if(id != team.id) return false;
        if(name != null ? !name.equals(team.name) : team.name != null) return false;
        
        return true;
    }
    
    @Override
    public int hashCode()
    {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(id).append(" | ");
        builder.append(name);
        return builder.toString();
    }
}

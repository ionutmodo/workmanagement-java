package com.wm.frames;

import com.wm.database.Database;
import com.wm.models.User;
import com.wm.repos.*;
import com.wm.services.*;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ionutvlad on 11.08.2016.
 */
public final class LoginFrame extends MainFrame
{
    private static LoginFrame instance;
    
    public static LoginFrame get()
    {
        if(instance == null)
            instance = new LoginFrame();
        return instance;
    }
    
    private JTextField emailTextField, passwordTextField;
    private JCheckBox showCheckBox;
    private JButton loginButton, exitButton;
    
    private LoginFrame()
    {
        super("WorkManagement - Login", SizesRepo.LOGIN_FRAME, null);
        
        setShowEvent();
        setLoginEvent();
        setExitEvent();
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        
        g.setColor(ColorsRepo.DARK_BLUE);
        g.setFont(FontsRepo.VERDANA_SMALL);
        g.fillRect(0, 0, SizesRepo.LOGIN_RECTANGLE_W, SizesRepo.LOGIN_RECTANGLE_H);
        g.drawString(StringsRepo.COPYRIGHT, PointsRepo.LOGIN_COPYRIGHT_X, PointsRepo.LOGIN_COPYRIGHT_Y);
        
        g.setColor(ColorsRepo.WHITE);
        g.setFont(FontsRepo.VERDANA_BIG);
        g.drawString(StringsRepo.APP_NAME, PointsRepo.LOGIN_TITLE_X, PointsRepo.LOGIN_TITLE_Y);
    }
    
    protected void createControls()
    {
        emailTextField = CompService.getTextField(PointsRepo.LOGIN_EMAIL_TEXT, SizesRepo.LOGIN_EMAIL_TEXT, false);
        passwordTextField = CompService.getTextField(PointsRepo.LOGIN_PASSWORD_TEXT, SizesRepo.LOGIN_PASSWORD_TEXT, true);
        showCheckBox = CompService.getCheckBox("Show", PointsRepo.LOGIN_SHOW_CHECK, SizesRepo.LOGIN_SHOW_CHECK);
        loginButton = CompService.getButton("Login", PointsRepo.LOGIN_LOGIN_BUTTON, SizesRepo.LOGIN_LOGIN_BUTTON);
        exitButton = CompService.getButton("Exit", PointsRepo.LOGIN_EXIT_BUTTON, SizesRepo.LOGIN_EXIT_BUTTON);
    }
    
    protected void addControls()
    {
        this.add(CompService.getLabel("Email", PointsRepo.LOGIN_EMAIL_LABEL, SizesRepo.LOGIN_EMAIL_LABEL));
        this.add(CompService.getLabel("Password", PointsRepo.LOGIN_PASSWORD_LABEL, SizesRepo.LOGIN_PASSWORD_LABEL));
        this.add(emailTextField);
        this.add(passwordTextField);
        this.add(showCheckBox);
        this.add(loginButton);
        this.add(exitButton);
    }
    
    private void setShowEvent()
    {
        showCheckBox.addActionListener(e ->
        {
            final boolean checked = showCheckBox.isSelected();
            ((JPasswordField) passwordTextField).setEchoChar(checked ? '\0' : '*');
        });
    }
    
    private void setLoginEvent()
    {
        loginButton.addActionListener(e ->
        {
            final String email = emailTextField.getText();
            final String password = passwordTextField.getText();
            final User user = Database.Update.loginUser(email, password);
        });
    }
    
    private void setExitEvent()
    {
        exitButton.addActionListener(e -> {
            LogService.get().close();
            System.exit(0);
        });
    }
}

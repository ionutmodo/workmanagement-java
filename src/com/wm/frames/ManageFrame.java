package com.wm.frames;

import com.wm.panels.manage.ManageDeleteUserPanel;
import com.wm.panels.manage.ManageNewProjectPanel;
import com.wm.panels.manage.ManageNewUserPanel;
import com.wm.repos.SizesRepo;
import javax.swing.*;
import java.awt.*;

/**
 * Created by ionutvlad on 15.08.2016.
 */
public final class ManageFrame extends MainFrame
{
    private static ManageFrame instance;
    
    public static ManageFrame get()
    {
        if(instance == null)
            instance = new ManageFrame();
        return instance;
    }
    
    private JMenuBar menuBar;
    private JMenu usersMenu, projectsMenu;
    private JMenuItem newUserItem, deleteUserItem;
    private JMenuItem newProjectItem;
    
    private ManageNewUserPanel newUserPanel;
    private ManageDeleteUserPanel deleteUserPanel;
    private ManageNewProjectPanel newProjectPanel;
    
    private ManageFrame()
    {
        super("WorkManagement - Management", SizesRepo.MANAGE_FRAME, new BorderLayout());
        
        deleteUserPanel.setVisible(true);
    }
    
    @Override
    protected void createControls()
    {
        newUserPanel = new ManageNewUserPanel();
        deleteUserPanel = new ManageDeleteUserPanel();
        newProjectPanel = new ManageNewProjectPanel();
        
        menuBar = new JMenuBar();
        usersMenu = new JMenu("Users");
        projectsMenu = new JMenu("Projects");
        
        newUserItem = new JMenuItem("New user");
        newUserItem.addActionListener(e ->
        {
            hidePanels();
            newUserPanel.setVisible(true);
            newUserPanel.populateTeamComboBox();
        });
        
        deleteUserItem = new JMenuItem("Delete user");
        deleteUserItem.addActionListener(e ->
        {
            hidePanels();
            deleteUserPanel.setVisible(true);
            deleteUserPanel.updateUsersList();
        });
        
        newProjectItem = new JMenuItem("New project");
        newProjectItem.addActionListener(e ->
        {
            hidePanels();
            newProjectPanel.setVisible(true);
        });
    }
    
    @Override
    protected void addControls()
    {
        usersMenu.add(newUserItem);
        usersMenu.add(deleteUserItem);
        menuBar.add(usersMenu);
    
        projectsMenu.add(newProjectItem);
        menuBar.add(projectsMenu);
    
        this.add(menuBar, BorderLayout.NORTH);
        this.add(newUserPanel, BorderLayout.CENTER);
        this.add(deleteUserPanel, BorderLayout.CENTER);
        this.add(newProjectPanel, BorderLayout.CENTER);
//        this.add(modifyProjectPanel, BorderLayout.CENTER);
    }
        
    private void hidePanels()
    {
        newUserPanel.setVisible(false);
        deleteUserPanel.setVisible(false);
        newProjectPanel.setVisible(false);
//        modifyProjectPanel.setVisible(false);
    }
}

package com.wm.frames;

import com.wm.repos.FontsRepo;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ionutvlad on 15.08.2016.
 */
public abstract class MainFrame extends JFrame
{
    public MainFrame(final String name, final Dimension size, final LayoutManager layout)
    {
        super(name);
        setup(size, layout);
        createControls();
        addControls();
    }
    
    private void setup(final Dimension size, final LayoutManager layout)
    {
        this.setPreferredSize(size);
        this.setLayout(layout);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setFont(FontsRepo.VERDANA_MEDIUM);
        this.pack();
        this.setLocationRelativeTo(null);
    }
    
    protected abstract void createControls();
    
    protected abstract void addControls();
}

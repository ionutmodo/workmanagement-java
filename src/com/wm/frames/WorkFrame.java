package com.wm.frames;

import com.wm.enums.OptionsList;
import com.wm.renderers.OptionsRenderer;
import com.wm.repos.*;
import com.wm.services.CompService;
import com.wm.services.LogService;
import com.wm.types.MessageType;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by ionutvlad on 28.08.2016.
 */
public final class WorkFrame extends MainFrame
{
    private static WorkFrame instance;

    public static WorkFrame get()
    {
        if(instance == null)
            instance = new WorkFrame();
        return instance;
    }

    private JList<OptionsList> optionsList;
    private JPanel leftPanel, rightPanel;
    private JButton activitiesButton, teamActivityButton, projectsButton, tasksButton;
    private JButton manageButton, optionsButton;

    private WorkFrame()
    {
        super(StringsRepo.WORK_TITLE, SizesRepo.WORK_FRAME, null);
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(ColorsRepo.DARK_BLUE);
        g.setFont(FontsRepo.VERDANA_SMALL);
        g.fillRect(0, 0, SizesRepo.WORK_RECTANGLE_W, SizesRepo.WORK_RECTANGLE_H);
        g.drawString(StringsRepo.COPYRIGHT, PointsRepo.WORK_COPYRIGHT_X, PointsRepo.WORK_COPYRIGHT_Y);
        g.setColor(ColorsRepo.WHITE);
        g.setFont(FontsRepo.VERDANA_BIG);
        g.drawString(StringsRepo.WORK_TITLE, PointsRepo.WORK_TITLE_X, PointsRepo.WORK_TITLE_Y);
        leftPanel.updateUI();
        rightPanel.updateUI();
    }

    private void createLeftPanel()
    {
        leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT))
        {
            @Override
            public void paintComponent(Graphics g)
            {
                g.setColor(ColorsRepo.DARK_BLUE);
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
        };
        leftPanel.setLocation(PointsRepo.WORK_LEFT_PANEL);
        leftPanel.setSize(SizesRepo.WORK_LEFT_PANEL);
        leftPanel.setBorder(BorderFactory.createLineBorder(ColorsRepo.WHITE, 1));
    }

    private void createRightPanel()
    {
        rightPanel = new JPanel(new FlowLayout(FlowLayout.CENTER))
        {
            @Override
            public void paintComponent(Graphics g)
            {
                g.setColor(ColorsRepo.DARK_BLUE);
                g.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
        };
        rightPanel.setLocation(PointsRepo.WORK_RIGHT_PANEL);
        rightPanel.setSize(SizesRepo.WORK_RIGHT_PANEL);
        rightPanel.setBorder(BorderFactory.createLineBorder(ColorsRepo.WHITE, 1));
    }

    private void createOptionsList()
    {
        optionsList = new JList<>(OptionsList.values());
        optionsList.setVisible(false);
        optionsList.setBackground(ColorsRepo.BRIGHT_BLUE);
        optionsList.setCellRenderer(new OptionsRenderer());
        optionsList.setLocation(PointsRepo.WORK_OPTIONS_LIST);
        optionsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        optionsList.setFont(FontsRepo.VERDANA_MEDIUM);
        optionsList.setSize(SizesRepo.WORK_OPTIONS_LIST);
        optionsList.addMouseListener(new OptionsListMouseListener());
    }

    @Override
    protected void createControls()
    {
        createLeftPanel();
        createRightPanel();
        createOptionsList();
        activitiesButton = CompService.getButton("Activities", null, null);
        teamActivityButton = CompService.getButton("Team activity", null, null);
        projectsButton = CompService.getButton("Projects", null, null);
        tasksButton = CompService.getButton("Tasks", null, null);
        manageButton = CompService.getButton("Manage", null, null);
        optionsButton = CompService.getButton("Options...", null, null);
        optionsButton.addActionListener(e -> optionsList.setVisible(!optionsList.isVisible()));
    }

    @Override
    protected void addControls()
    {
        leftPanel.add(activitiesButton);
        leftPanel.add(teamActivityButton);
        leftPanel.add(projectsButton);
        leftPanel.add(tasksButton);
        leftPanel.add(manageButton);
        rightPanel.add(optionsButton);
        this.add(leftPanel);
        this.add(rightPanel);
        this.add(optionsList);
    }

    private class OptionsListMouseListener extends MouseAdapter
    {
        private void act(final int index)
        {
            OptionsList option = OptionsList.values()[index];
            switch(option)
            {
                case Profile:
                    break;
                case Info:
                    break;
                case Logout:
                    break;
                case Exit:
                    break;
            }
        }

        @Override
        public void mouseClicked(MouseEvent e)
        {
            JList<OptionsList> list = (JList<OptionsList>) e.getSource();
            OptionsList value = list.getSelectedValue();
            int index = list.locationToIndex(new Point(e.getX(), e.getY()));
            LogService.get().log(MessageType.INFO, "Selected " + value.toString() + " option #" + index);
            act(index);
            list.setVisible(false);
        }
    }
}
